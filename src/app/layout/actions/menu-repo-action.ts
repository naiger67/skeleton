import { Injectable } from "@angular/core";
import { ApiGateway } from 'src/app/api-gateway/http/api-gateway';
import { ApiGatewayCommand } from 'src/app/api-gateway/http/api-gateway-command';
import { environment } from 'src/environments/environment';
import { AuthSession } from 'src/app/identity/services/auth-session';

@Injectable({ providedIn: 'root' })
export class MenuRepoAction {

	constructor(
		private authSession: AuthSession,
		private apiGateway: ApiGateway) { }

	execute(params) {

		let command = new ApiGatewayCommand({
			url: environment.security.authorizacion.menu,
			method: ApiGatewayCommand.METHOD_POST,
			params: this.authSession.toMenuRequest(),
			headers: {},
			debug: ApiGatewayCommand.DEBUG_ON,
			response: this.apiGateway.responseSuccessFake([{
				id: 100,
				uiParentId: null,
				uiLevel: 1,
				uiName: 'Modulo Demo',
				icon: '',
				url: '',
				childrens: [],
			}, {
				id: 101,
				uiParentId: 100,
				uiLevel: 2,
				uiName: 'Componente Demo',
				icon: '',
				url: 'main/demo',
				childrens: []
			}])
		});

		return this.executeCommand(command);
	}

	private executeCommand(command) {
		return this.apiGateway.execute(command);
	}
}
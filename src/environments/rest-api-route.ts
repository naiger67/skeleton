export class RestApiRoute {

	private ip;
	private puerto;
	private namespace;
	private protocol;
	private domain;
	private baseUrl;

	public static make(ip, puerto, namespace?, protocol?) {
		let protocolAttr = protocol || "https";
		let namespaceAttr = namespace || "";
		return new RestApiRoute(ip, puerto, namespaceAttr, protocolAttr);
	}

	constructor(ip, puerto, namespace, protocol) {
		this.ip = ip;
		this.puerto = puerto;
		this.namespace = namespace;
		this.protocol = protocol;
		this.setDomain();
		this.setBaseUrl();
	}

	private setDomain() {
		this.domain = [this.ip, this.puerto].join(":");
	}

	public getDomain() {
		return this.domain;
	}

	private setBaseUrl() {
		let namespace = this.namespace != "" ? this.namespace + "/" : "";
		this.baseUrl = this.protocol + "//" + this.domain + "/" + namespace + "/";
	}

	public route(endpoit) {
		return this.baseUrl + endpoit;
	}

}
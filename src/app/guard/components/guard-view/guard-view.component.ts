import { Component, OnInit, Input } from '@angular/core';
import { AuthorizerService } from 'src/app/identity/services/authorizer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-guard-view',
  templateUrl: './guard-view.component.html',
  styleUrls: ['./guard-view.component.css'],
})
export class GuardViewComponent implements OnInit {
  
  @Input() anyRol?: any;
  @Input() allRol?: any;

  constructor(
    private router: Router,
    private authorizerService: AuthorizerService) { }

  ngOnInit() {
    let hasAnyRol = !this.anyRol ? true : this.authorizerService.hasAnyRol(this.anyRol);
    let hasAllRol = !this.allRol ? true : this.authorizerService.hasAllRol(this.allRol);
    if(!hasAnyRol || !hasAllRol) this.redirectToMain(); 
  }

  redirectToMain() {
    this.router.navigate(["/forbidden"]);
  }
}

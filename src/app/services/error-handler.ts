import {Injectable} from '@angular/core';
import { Logger } from 'src/app/services/logger';
import { RedirectErrorHandler } from './redirect-error-handler';
@Injectable({providedIn: 'root'})
export class ErrorHandler {
  
  constructor(
    private redirectErrorHandler: RedirectErrorHandler,
    private logger: Logger) {}
  
  public handle(context, method) {
    let next = method.bind(context);

    return (error) => {
      //if(this.isErrorServer(error)) this.redirectToErrorPage(error);
      //else next(error);
      return next(error);
    }

  }

  private isErrorServer(error) {
    return [300, 400, 401, 403, 500].includes(error.status());
  }

  private redirectToErrorPage(error) {
    this.logger.log('redirectToErrorPage:', {});
    this.redirectErrorHandler.redirectTo(error);
  }
}
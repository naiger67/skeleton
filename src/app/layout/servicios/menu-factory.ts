import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class MenuFactory {

	constructor() {}

	public create(menu) {
		let _menu = this.menuMapHasSub(menu);
		if (_menu.length) _menu = this.menuForEachLast(_menu);
		return _menu;
	}

	private menuMapHasSub(menu) {
		return menu.map((item) => {
			item.className = item.childrens.length ? "has-sub" : "";
			if (item.childrens) {

				item.childrens = item.childrens.map((item) => {
					item.className = item.childrens.length ? "has-sub" : "";
					if (item.childrens) {

						item.childrens = item.childrens.map((item) => {
							item.className = item.childrens.length ? "has-sub" : "";
							if (item.childrens) {

								item.childrens = item.childrens.map((item) => {
									item.className = item.childrens.length ? "has-sub" : "";
		
									return item;
								});
							}
							return item;
						});
					}

					return item;
				});
			}
			return item;
		});
	}

	private menuForEachLast(menu) {
		menu.forEach(item => {
			if (item.childrens.length) {
				let lastElement = item.childrens[item.childrens.length - 1];
				this.addLastClassName(lastElement);

				item.childrens.forEach(item => {
					if (item.childrens.length) {
						let lastElement = item.childrens[item.childrens.length - 1];
						this.addLastClassName(lastElement);

						item.childrens.forEach(item => {
							if (item.childrens.length) {
								let lastElement = item.childrens[item.childrens.length - 1];
								this.addLastClassName(lastElement);
							}
						});
					}
				});
			}
		});

		return menu;
	}

	private addLastClassName(lastElement) {
		let className = "last";
		if (lastElement.className == "has-sub") className = lastElement.className;
		lastElement.className = className;
	}

}
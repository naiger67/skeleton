import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
@Injectable({providedIn: 'root'})
export class ModalFactory {
    
    constructor(private modalService: NgbModal) {}

    make(component, context, close, dismiss) {
        const modalRef = this.modalService.open(component);
        modalRef.componentInstance.context = context;
        modalRef.result.then(close, dismiss);
    }
}
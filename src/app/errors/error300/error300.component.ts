import { Component, OnInit } from '@angular/core';
import { RedirectErrorHandler } from 'src/app/services/redirect-error-handler';

@Component({
  selector: 'app-error300',
  templateUrl: './error300.component.html',
  styleUrls: ['./error300.component.css']
})
export class Error300Component implements OnInit {

  constructor(
    private flashMessage: RedirectErrorHandler) { }

  ngOnInit() {
    
  }

}

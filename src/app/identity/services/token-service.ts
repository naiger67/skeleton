import { Injectable } from '@angular/core';
import { AuthSession } from '../services/auth-session';
import { environment } from '../../../environments/environment'


@Injectable({providedIn: 'root'})
export class TokenService {
  constructor(private authSession: AuthSession) {}

  getToken() {
    return this.getTokenAsync();
  }

  private getTokenAsync() {
    return this.authSession.authToken();
  }

  whitelistedDomains() {
    return environment.security.domainRegister;
  }

  blacklistedRoutes() {
    return [];
  }

  toConfig() {
    return {
      tokenGetter:this.getToken.bind(this),
      whitelistedDomains: this.whitelistedDomains(),
      blacklistedRoutes: this.blacklistedRoutes()
    }
  }

}
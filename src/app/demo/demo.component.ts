import { Component, OnInit } from '@angular/core';
import { GuardConfig } from '../identity/services/authorizer.config';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  public textoProtegido;
  public textoDescripcion;
  
  constructor(public guard: GuardConfig) { }

  ngOnInit() {
    this.setTexto();
  }

  private setTexto() {
    this.textoProtegido = "Esto se mostrara para los usuario administradores";
    this.textoDescripcion = "Si eres administrador: Te saludos con un alert!";
  }

  onClickTest() {
    if(!this.guard.valid['guardA']) return;
    alert("hola admin");
  }
}

import { Injectable } from '@angular/core';
import { AuthSession } from '../services/auth-session';
import { HttpClient } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { throwError, of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LogoutService {

	private static fake = true;
	
	constructor(private authSession: AuthSession, private http: HttpClient) { }

	private url() { 
		return ""; 
	}

	private headers() { 
		return {}; 
	}

	private params(form) {
		return {};
	}

	execute(form) {
		let config = { headers: this.headers(), params: this.params(form)};

		if(LogoutService.fake) return of(true);

		return this.http.post(this.url(), {}, config).pipe(
			tap((response)=>{
				this.authSession.clear();
			}),
			catchError((error)=>{
				return throwError(error)
			})
		);
	}
}
import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild } from '@angular/router';
import { AuthSession } from './auth-session';

@Injectable({ providedIn: 'root'})
export class AuthGuardService implements CanActivateChild {

  constructor(
      public authSession: AuthSession, 
      public router: Router) {}

  canActivateChild(): boolean {

    if (!this.authSession.isAuthenticated()) {
      this.router.navigateByUrl('/login');
      return false;
    }

    return true;
  }
}
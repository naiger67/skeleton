import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error300Component } from './errors/error300/error300.component';
import { Error401Component } from './errors/error401/error401.component';
import { Error404Component } from './errors/error404/error404.component';
import { Error500Component } from './errors/error500/error500.component';
import { DashboardComponent } from './layout/components/dashboard/dashboard.component';
import { LoginComponent } from './identity/components/login/login.component';
import { AuthGuardService } from './identity/services/auth-guard.service';
import { DemoComponent } from './demo/demo.component';

const routes: Routes = [
  { path: '', redirectTo: "/login", pathMatch: 'full' },
  //-------------Errors------------------------------------------
  { path: 'redirect-error', component: Error300Component },
  { path: 'forbidden', component: Error401Component },
  { path: 'not-found', component: Error404Component },
  { path: 'server-error', component: Error500Component },

  //login
  { path: 'login', component: LoginComponent },

  //layout
  { 
    path: 'main' , 
    component: DashboardComponent,
    canActivateChild: [AuthGuardService],
    children: [
      { path: 'demo', component: DemoComponent }
    ]
 },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DemoComponent } from './demo.component';
import { GuardModule } from '../guard/guard.module';
@NgModule({
  declarations: [DemoComponent],
  imports: [
    CommonModule,
    FormsModule, 
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    GuardModule
  ],
  providers: [],
  exports: [],
  entryComponents: []
})
export class DemoModule { 
  constructor() {}
}

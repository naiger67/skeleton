import { Component, OnInit } from '@angular/core';
import { MenuRepoAction } from '../../actions/menu-repo-action';
import { MenuFactory } from '../../servicios/menu-factory';
import { Router } from '@angular/router';
import { LogoutService } from 'src/app/identity/async-services/logout.service';
import { Beta } from 'src/app/services/beta';
import { AuthSession } from 'src/app/identity/services/auth-session';
import { MenuUIFactoryService } from '../../servicios/menu-ui-factory-service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public menu;

  constructor(
    private beta: Beta,
    private menuUIFactoryService: MenuUIFactoryService,
    private logoutService: LogoutService,
    private menuRepoAction: MenuRepoAction, 
    private menuFactory: MenuFactory,
    private authSession: AuthSession,
    private router: Router) { }

  ngOnInit() {
    this._menuRepoAction();
    this.onUpdateMenu();
  }

  private onUpdateMenu() {
    this.beta.on('menu.update', (options)=>{
      this.menu = options;
    });
  }

  private _menuRepoAction() {
    this.menuRepoAction.execute({}).subscribe(
      this.onHorizontalMenuRepoActionSuccess.bind(this),
      this.onHorizontalMenuRepoActionError.bind(this)
    );
  }

  private onHorizontalMenuRepoActionSuccess(response) {
    let data = response.data();
    let treeMenu = this.menuUIFactoryService.create(data);
    this.menu = this.menuFactory.create(treeMenu);
  }

  private onHorizontalMenuRepoActionError(error) {
    let data = error.data();
    console.log("onHorizontalMenuRepoActionError", data);
  }

  redirectToView(item){
    if (item.url === "") return;
    this.router.navigateByUrl(item.url);
  }

  logout() {
    this._logout();
  }

  private _logout() {
    this.logoutService.execute({}).subscribe(
      this.onLogoutSuccess.bind(this),
      this.onLogoutError.bind(this)
    );
  }
  
  private onLogoutSuccess(response) {
    this.router.navigateByUrl("/login").then(()=>{
      this.authSession.clear();
    });
  }

  private onLogoutError(error) {
    console.log("onLogoutError", error);
  }
}

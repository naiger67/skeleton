import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class MenuUIFactoryService {

	constructor() { }

	/** CREATE TREE FROM ARRAY */
	create(permisos) {

		if(!permisos.length) return [];

		let permisoChecked = this.addCheckedPropToAllPermisos(permisos);
		let levels = this.getLevels(permisoChecked);
		let deepTree = this.deepTree(permisos);
		levels[deepTree] = this.mapLastLevel(levels[deepTree]);

		if(deepTree == 1) return levels[deepTree]
		
		let tree = [];
		for (let i = deepTree; i >= 2; i--) {
			let levelA = levels[i] || [];
			let levelB = levels[i - 1] || [];
			if (levelA.length > 0 && levelB.length > 0) tree = this.buildTree(levelB, levelA);
		}
		return tree;
	}

	private addCheckedPropToAllPermisos(permisos) {
		return permisos;
		return permisos.map(permiso => {
			permiso.checked = false;
			return permiso;
		});
	}

	private getLevels(permisos) {
		return permisos.reduce((carry, permiso) => {
			if (!carry[permiso.uiLevel]) carry[permiso.uiLevel] = [];
			carry[permiso.uiLevel].push(permiso);
			return carry;
		}, {});
	}

	private deepTree(permisos) {
		return permisos.reduce((carry, permiso) => {
			if (permiso.uiLevel > carry) carry = permiso.uiLevel;
			return carry;
		}, 0);
	}


	private mapLastLevel(permisos) {
		return permisos.map(x => {
			x.childrens = [];
			return x;
		});
	}

	private buildTree(levelA, levelB) {
		return levelA.map(permiso => {
			permiso.childrens = levelB.filter(x => x.uiParentId == permiso.id);
			return permiso;
		});
	}
	/** CREATE TREE FROM ARRAY */

	/** FLAT ARRAY INTERFACE */
	getPermisosChecked(tree) {
		let permisosChecked = [];
		this._flatTree(permisosChecked, tree);
		return permisosChecked.filter(x => x.checked == true);
	}

	flatTree(tree) {
		let permisosChecked = [];
		this._flatTree(permisosChecked, tree);
		return permisosChecked;
	}

	private _flatTree(carry, menuTree) {
		menuTree.forEach(x => {
			if (!x.childrens.length) carry.push(x);
			else {
				let childrens = x.childrens;
				x.childrens = [];
				carry.push(x);
				this._flatTree(carry, childrens);
			}
		});
	}
	/** FLAT ARRAY INTERFACE */

	//TO LEVELS INTERFACE
	toLevels(permisos) {
		return {
			levels: this.getLevels(permisos),
			deep: this.deepTree(permisos)
		}
	}
	//TO LEVELS INTERFACE
}
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuardViewComponent } from './components/guard-view/guard-view.component';

@NgModule({
  declarations: [
    GuardViewComponent
  ],
  exports: [
    GuardViewComponent
  ],
  imports: [
    CommonModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GuardModule { }

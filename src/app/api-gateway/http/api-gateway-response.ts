import { of } from 'rxjs';

export class ApiGatewayResponse {

	private _code: number;
	private _message: string;
	private _data;
	private _status;

	static make(body, status) {
		return new ApiGatewayResponse(
			body.code,
			body.message,
			body.data,
			status
		);
	}

	static makeOkFromData(data) {
		return of(new ApiGatewayResponse(1000, 'Ok', data, 200));
	}

	constructor(code, message, data, status) {
		this._code = parseInt(code);
		this._message = message;
		this._data = data;
		this._status = status;
	}

	code() { return this._code; }

	message() { return this._message; }

	data() { return this._data; }

	status() { return this._status; }

	hasError() {
		return this._code > 1000 || this._status > 200;
	}

	hasErrorAndEqual(code) {
		return this.hasError() && this._code == code;
	}

	unWrap() {
		return {
			code: this._code,
			data: this._data,
			message: this._message,
			status: this._status
		}
	}

	toErrorLogin() {
		return {
			status: this._code,
			error: this._data,
			message: this._message,
		}
	}

} 
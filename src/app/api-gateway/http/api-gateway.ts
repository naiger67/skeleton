import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ApiGatewayCommand } from './api-gateway-command';
import { ApiGatewayResponse } from './api-gateway-response';
import { map, catchError } from 'rxjs/operators'
import { throwError, Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class ApiGateway {

	constructor(private http: HttpClient) { }

	public execute(command: ApiGatewayCommand): Observable<ApiGatewayResponse> {
		return command.debug()
			? command.responseFake()
			: this.asyncAction(command);
	}

	private asyncAction(command) { 
		let action = this[command.method()].bind(this);
		let observable = action(command);
		return observable.pipe(
			map((response: HttpResponse<any>) => {
				let apiGatewayResponse = ApiGatewayResponse.make(response.body, response.status);
				this.assertThatResponseNotHasError(apiGatewayResponse);
				return apiGatewayResponse;
			}),
			catchError((errorResponse: HttpErrorResponse) => {
				let apiResponseError = new ApiGatewayResponse(
					errorResponse.status,
					errorResponse.error,
					errorResponse.message,
					errorResponse.status
				);

				return throwError(apiResponseError);
			})
		);
	} 

	private assertThatResponseNotHasError(apiGatewayResponse:ApiGatewayResponse) {
		if (!apiGatewayResponse.hasError()) return;
		throw apiGatewayResponse.toErrorLogin();
	}

	public get(command) {
		return this.http.get(command.url(), command.configGet());
	}

	public post(command) {
		return this.http.post(command.url(), command.params(), command.configPost());
	}

	responseSuccessFake(dummyData) {
		let message = 'Success Fake Response With Dummy Data';
		return this.responseFake("00", message, dummyData, 200);
	}

	responseErrorLogicFake(dummyData) {
		let message = 'Error LOGIC Fake Response With Dummy Data';
		return this.responseFake("01", message, dummyData, 200);
	}

	responseErrorServerFake(dummyData) {
		let message = 'Error SERVER Fake Response With Dummy Data';
		return this.responseFake("012", message, dummyData, 500);
	}

	responseFake(code, msg, data, status) {
		return new ApiGatewayResponse(code, msg, data, status);
	}
}


export class Select {
    public id;
    public list;
    public selected;
    public listEmpty;
  
    constructor() {
      this.id = '';
      this.list = [];
      this.selected = {};
      this.listEmpty = true;
    }
  
    render(list) {
      this.setList(list);
      this.setId();
      this.setSelected();
      this.setListEmpty();
    }
  
    private setList(list) {
      this.list = list;
    }
  
    private setId() {
      if(!this.list.length) return;
      this.id = this.list[0].id;
    }
  
    private setSelected() {
      if(!this.list.length) return;
      this.selected = this.list.filter(x=>{
        return x.id == this.id;
      })[0];
    }
  
    private setListEmpty() {
      this.listEmpty = this.list.length ? true: false;
    }
  
    onChange() {
      this.setSelected();
    }
  }
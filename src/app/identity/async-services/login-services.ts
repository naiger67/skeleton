import { Injectable } from '@angular/core';
import { AuthSession } from '../services/auth-session';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { catchError, tap } from 'rxjs/operators';
import { throwError, of } from 'rxjs';
import { AuthorizerService } from '../services/authorizer.service';

@Injectable({ providedIn: 'root' })
export class LoginServices {

	private static URL = environment.security.oauth2.tokenUrl;
	private static AUTHORIZATION = environment.security.oauth2.authorizacionBasic;
	private static GRANT_TYPE = environment.security.oauth2.grantType;

	constructor(
		private authorizerService: AuthorizerService,
		private authSession: AuthSession, 
		private http: HttpClient) { }

	private url() { return LoginServices.URL }

	private headers() { return { "authorization": LoginServices.AUTHORIZATION } }

	private params(form) {
		return {
			"grant_type": LoginServices.GRANT_TYPE,
			"username": form.username,
			"password": form.password
		};
	}

	execute(form) {
		if(environment.security.debug.on) return this.executeFakeSuccess();

		let config = {
			headers: this.headers(),
			params: this.params(form)
		};

		return this.http.post(this.url(), {}, config).pipe(
			tap((response: any)=>{
				this.authSession.setAuthToken(response.access_token);
				this.authSession.setRefreshToken(response.refresh_token);
				this.hasAuthorization()
			}),

			catchError((error)=>{
				this.authSession.clear();
				return throwError(error);
			})
		);
	}

	private executeFakeSuccess() {
		let response = {
			access_token: environment.security.debug.accessToken,
			refresh_token: environment.security.debug.refreshToken
		};

		this.authSession.setAuthToken(response.access_token);
		this.authSession.setRefreshToken(response.refresh_token);
		
		return of(response);
	}

	private hasAuthorization() {
		if(!this.authorizerService.authorized()) throw "Forbidden";
	}

}
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
@Injectable({providedIn: 'root'})
export class RedirectErrorHandler {
    
    private _redirectUrl;
    private _responseError;

    constructor(private router: Router) {}

    redirectTo(responseError) {
        this.setResponseError(responseError);
        this.setRedirectUrl();
        return this.router.navigate([this._redirectUrl]);
    }

    private setResponseError(responseError) {
        this._responseError = responseError;
    }

    private setRedirectUrl() {
        let dictionary = this.errorDictionary();
        let status = this._responseError.status();
        let statusDefault = 404;

        this._redirectUrl = dictionary[status] 
            ? dictionary[status]
            : dictionary[statusDefault];
    }

    private errorDictionary() {
        return {
            300: 'redirect-error',
            401: 'forbidden',
            404: 'not-found',
            500: 'server-error'
        };
    }

    responseError() {
        return this._responseError;
    }
}
import { Injectable } from '@angular/core';
import { AuthorizerService } from './authorizer.service';

@Injectable({providedIn: 'root'})
export class GuardConfig {

  public valid;
  
  constructor(private authorizerService: AuthorizerService) { 
		this.setValid();
	}

	private setValid() {
    this.valid = {
      'guardA': this.authorizerService.hasOnceRol("admin"),
      'guardB': this.authorizerService.hasAnyRol(["admin", "comercios_afiliados"]),
      'guardC': this.authorizerService.hasAllRol(["inventario", "pagos"])
    };
	}
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Error300Component } from './error300/error300.component';
import { Error401Component } from './error401/error401.component';
import { Error404Component } from './error404/error404.component';
import { Error500Component} from './error500/error500.component';
@NgModule({
  declarations: [
    Error300Component,
    Error401Component,
    Error404Component,
    Error500Component,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [],
  entryComponents: []
})
export class ErrorsModule { }

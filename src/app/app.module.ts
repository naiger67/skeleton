import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { ApiGatewayModule } from './api-gateway/api-gateway.module';
import { IdentityModule } from './identity/identity.module';
import { TokenService } from './identity/services/token-service';
import { JwtModule, JWT_OPTIONS, }  from '@auth0/angular-jwt';
import { ErrorsModule } from './errors/errors.module';
import { LayoutModule } from './layout/layout.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { DemoModule } from './demo/demo.module';


export function jwtOptionsFactory(tokenService) {
  return tokenService.toConfig();
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ApiGatewayModule,
    NgbModule,
    NgbCarouselModule,
    IdentityModule,
    ErrorsModule,
    LayoutModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DemoModule,
    ToastrModule.forRoot(),
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [TokenService]
      }
    })
  ],
  providers: [
    TokenService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

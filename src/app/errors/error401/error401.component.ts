import { Component, OnInit, Renderer2, Renderer } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-error401',
  templateUrl: './error401.component.html',
  styleUrls: ['./error401.component.scss']
})
export class Error401Component implements OnInit {

  constructor(
    private router: Router,
    private renderer: Renderer) { }

  ngOnInit() {
    this.renderer.setElementClass(document.body, 'login-bg', true);
  }

  volver() {
    this.router.navigateByUrl("/main");
  }

}
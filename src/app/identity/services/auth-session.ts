import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.quality';

@Injectable({ providedIn: 'root' })
export class AuthSession {

	private _authTokenKey = 'AUTH_TOKEN_KEY';
	private _refreshTokenKey = 'REFRESH_TOKEN_KEY';
	private payload;

	constructor() { }

	setAuthToken(authToken) {
		localStorage.setItem(this._authTokenKey, authToken);
	}

	setRefreshToken(refreshToken) {
		localStorage.setItem(this._refreshTokenKey, refreshToken);
	}

	authToken() {
		return localStorage.getItem(this._authTokenKey);
	}

	refreshToken() {
		return localStorage.getItem(this._refreshTokenKey);
	}

	isAuthenticated() {
		return this.authToken() && this.refreshToken();
	}

	getAuthUser() { }

	private decodeToken() {
		let authTokenArray = this.authToken().split(".");
		let payloadJsonStringify = atob(authTokenArray[1]);
		let payload = JSON.parse(payloadJsonStringify);
		let data = payload.data;
		return data;
	}

	authorization() {
		let payload = this.decodeToken();
		let roles = payload.roles || [];
		return {roles: roles};
	}

	toMenuRequest() {
		let payload = this.decodeToken();
		return {}
	}

	clear() {
		let keys = [this._authTokenKey, this._refreshTokenKey];
		keys.forEach((key) => {
			localStorage.removeItem(key);
		});
	}
}
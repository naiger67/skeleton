import { Component, ViewEncapsulation, Renderer, ɵConsole } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginServices } from '../../async-services/login-services';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent {

  public loginForm: FormGroup;

  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private renderer: Renderer,
    private loginServices: LoginServices,
    private router: Router) {
    this.renderer.setElementClass(document.body, 'login-bg', true);
  }

  ngOnInit() {
    this.setLoginForm();
  }

  ngOnDestroy() {
    this.renderer.setElementClass(document.body, 'login-bg', false);
  }

  private setLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  login() {
    this._login(this.loginForm.getRawValue());
  }

  private _login(command) {
    this.loginServices.execute(command).subscribe(
      this.onLoginSuccess.bind(this),
      this.onLoginError.bind(this)
    );
  }

  private onLoginSuccess(response) {
    this.redirectToDashboard();
  }

  private onLoginError(error) {
    this.showInvalidPassword();
  }

  private redirectToDashboard() {
    setTimeout(() => this.router.navigate(['main']), 1000);
  }

  showInvalidPassword() {
    this.toastr.toastrConfig.progressBar = true;
    this.toastr.warning('Usuario o contraseña invalida', 'Login:', {
      positionClass: 'toast-bottom-center'
    });
    this.toastr.toastrConfig.preventDuplicates = true;
  }

}

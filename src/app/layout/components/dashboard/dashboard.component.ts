import { Component, OnInit, ViewEncapsulation, Renderer } from '@angular/core';
import { Router } from '@angular/router';
import { AuthSession } from 'src/app/identity/services/auth-session';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent {

  constructor(
    private renderer: Renderer, 
    private router: Router, 
    private authSession: AuthSession) {

    this.renderer.setElementClass(document.body, 'dashboard-bg', true);
  }

  ngOnInit() {
    this.guard();
  }

  private guard() {
    if(this.authSession.isAuthenticated()) return;

    this.router.navigateByUrl("/login").then(()=>{
      this.authSession.clear();
    });
  }

  ngOnDestroy() {
    this.renderer.setElementClass(document.body, 'dashboard-bg', false);
  }
}

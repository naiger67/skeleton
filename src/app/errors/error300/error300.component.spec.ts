import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Error300Component } from './error300.component';

describe('Error300Component', () => {
  let component: Error300Component;
  let fixture: ComponentFixture<Error300Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Error300Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Error300Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

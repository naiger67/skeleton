import { Injectable, EventEmitter } from '@angular/core';

@Injectable({providedIn: 'root'})
export class Beta { 
  private channels;

  constructor() {
    this.channels = [];
  }

  private addSubscribers(channel, subscriber) {
    let eventEmitter = new EventEmitter();
    eventEmitter.subscribe(subscriber);
    let channelX = this.findOrNewChannel(channel);
    channelX.subscribers = [eventEmitter];
  }

  private findOrNewChannel(channel) {
    let channels = this.channels.filter(s=>s.channel == channel);
    if(channels.length) return channels[0];

    let nuevoChannel = {
      channel: channel,
      subscribers: []
    };

    this.channels.push(nuevoChannel);

    return nuevoChannel;
  }

  dispose() {
    this.channels.forEach(channelI => {
      channelI.subscribers.forEach(subscriberI=>{
        subscriberI.unsubscribe();
      });
    });
  }

  emit(channel, data) {
    let channelX = this.findOrNewChannel(channel);
    channelX.subscribers.forEach(subscriberI=>{
      subscriberI.emit(data);
    });
  }

  on(channel, cb) {
    this.addSubscribers(channel, cb);
  }
}
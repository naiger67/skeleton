import {Injectable} from '@angular/core';
@Injectable({providedIn: 'root'})
export class SuccessHandler {
  constructor() {}
  public handle(context, method) {
    return method.bind(context);
  }
}
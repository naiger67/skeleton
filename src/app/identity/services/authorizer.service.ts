import { Injectable } from '@angular/core';
import { AuthSession } from './auth-session';
import * as lodash from 'lodash';

@Injectable({providedIn: 'root'})
export class AuthorizerService {

    constructor(private authSession: AuthSession) {}

    hasAnyRol(roles) {
			let authorization = this.authSession.authorization();
			if(!authorization.roles) return false;
			let filterRoles = this.filterRoles(authorization, roles);
			return filterRoles.length > 0;
		}

		hasAllRol(roles) {
			let authorization = this.authSession.authorization();
			if(!authorization.roles) return false;
			let filterRoles = this.filterRoles(authorization, roles);
			return filterRoles.length == roles.length;
		}

		hasOnceRol(rol) {
			return this.hasAnyRol([rol]);
		}

		private filterRoles(authorization, roles) {
			let rolesFlatten = lodash.flattenDeep(roles);
			return authorization.roles.filter((rol)=>rolesFlatten.indexOf(rol) > -1);
		}
		
		authorized() {
			let authorization = this.authSession.authorization();
			return authorization.roles.length > 0;
		}
}
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class Logger {
    private _debug: boolean;
    constructor() {
        this._debug = false;
    }

    log(key, value) {
        if(!this._debug) return;
        let args = Array.from(arguments);
        console.log.apply(console, args);
    }
}
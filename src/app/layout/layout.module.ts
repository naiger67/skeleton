import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { ApiGatewayModule } from '../api-gateway/api-gateway.module';


@NgModule({
  declarations: [
    NavbarComponent, 
    DashboardComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    AppRoutingModule
  ]
})
export class LayoutModule { }
